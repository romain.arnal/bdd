#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define REQUEST_SIZE 400
#define NB_MOT_MAX 10
#define LG_MOT 100

int fichier_existe(char *fichier){
    FILE* file;
    if((file = fopen(fichier,"r"))){
        fclose(file);
        return 1;
    }
    return 0;
}

int createtable(char **mots, int c, int socketClient){
    if (c<3){
        send(socketClient,"Nombre argument non valide",REQUEST_SIZE,0);
        return 0;
    } 
    //On rajoute .txt au nom de la table 

    char *table = calloc(LG_MOT,sizeof(char));
    int i = 0;
    while(*(mots[2]+i)!='\0' && *(mots[2]+i)!='\n'){
        *(table+i)=*(mots[2]+i);
        i++;
    }
    table[i]='\0';
    strcat(table,".txt");
    //On fait des testes d'erreures de saisies
    if (strcmp(mots[1],"TABLE")){
        send(socketClient,"Mauvais format : absence de 'TABLE' en 2eme position",REQUEST_SIZE,0);
    } else if (fichier_existe(table)){
        send(socketClient,"Opération impossible : La table existe déjà",REQUEST_SIZE,0);
    } else {
        // si pas d'erreur 
        FILE* ftable = fopen(table,"w");
        fputs("0,",ftable);
        fputs(table,ftable);
        fputs("\n",ftable);
        fclose(ftable);
        send(socketClient,"Table créée",REQUEST_SIZE,0);
    }
    free(table);
    return 1;
}

int droptable(char **mots, int c, int socketClient){
    if (c<3){
        printf("Nombre argument non valide\n");
        return 0;
    }
    //On rajoute .txt au nom de la table 
    char *table = malloc(sizeof(char)*LG_MOT);
    int i = 0;
    while(*(mots[2]+i)!='\0' && *(mots[2]+i)!='\n'){
        *(table+i)=*(mots[2]+i);
        i++;
    }
    table[i]='\0';
    strcat(table,".txt");
    //On fait des testes d'erreures de saisies
    if (strcmp(mots[1],"TABLE")){
        printf("Mauvais format : absence de 'TABLE' en 2eme position\n");
    } else if (!fichier_existe(table)){
        printf("Opération impossible : La table n'existe pas\n");
    } else {
        // si pas d'erreur 
        remove(table);
    }
    free(table);
    return 1;
}

int value(char* mot, char *vars[3], int socketClient){
    if(*mot!='('){
        printf("pas de '('\n");
        return 0;
    } 
    int i=1;
    int nb_var = 0;
    int c=0;
    while(*(mot+i)!=')'){
        if (*(mot+i)=='\n' || *(mot+i)=='\0'){
            printf("erreur format\n");
            return 0;
        }
        if (*(mot+i)==','){
            *(vars[nb_var]+c)='\0';
            nb_var+=1;
            if(nb_var==3){
                return 0;
            }
            c=0;
        } else {
            *(vars[nb_var]+c)=*(mot+i);
            c++;
        }
        i++;
    }
    if(nb_var==2){
        return 1;
    }
    return 0;
}

void indice_next(char* ligne, char* var){
    int i = 0;
    while(*(ligne+i)!=','){
        *(var+i)= *(ligne+i);
        i++;
    }
}

void insert(char **mots, int c, int socketClient){
    if(c<5){
        printf("Pas assez d'arguments\n");
    } else {
         strcat(mots[2],".txt");
        //On fait des testes d'erreures de saisies
        if (strcmp(mots[1],"INTO")){
            printf("Mauvais format : absence de 'INTO' en 2eme position\n");
        } else if (!fichier_existe(mots[2])){
            printf("Opération impossible : La table n'existe pas\n");
        } else if(strcmp(mots[3],"VALUE")){
            printf("Mauvais format : absence de 'VALUE' en 4eme position\n");
        } else {
            // si pas d'erreur
            // test sur les valeurs des variables
            char* var1 = calloc(LG_MOT,sizeof(char));
            char* var2 = calloc(LG_MOT,sizeof(char));
            char* var3 = calloc(LG_MOT,sizeof(char));
            char* vars[3] = {var1,var2,var3};
            int nb;
            int i=0;


            if(value(mots[4],vars,socketClient)) {
                FILE* f = fopen(mots[2],"r");
                char *ligne = NULL;
                size_t len=0;
                char *ind = malloc(10*sizeof(char));
                while (getline(&ligne,&len,f) != -1){
                    indice_next(ligne,ind);
                }
                fclose(f);
                f = fopen(mots[2],"a");
                int in = atoi(ind);
                in++;
                sprintf(ind, "%d", in);
                fputs(ind,f);
                fputs(",",f);
                fputs(var1,f);
                fputs(",",f);
                fputs(var2,f);
                fputs(",",f);
                fputs(var3,f);
                fputs("\n",f);
                free(ind);
                fclose(f);
            } else {
                printf("Erreur de récupération des données\n");
            }
            free(var1);
            free(var2);
            free(var3);
        }
    }   
}

void detect_var(char* colonne, int arg[3]){
    char* var = calloc(LG_MOT,sizeof(char));
    int i = 0;
    int c = 0;
    while(*(colonne+i)!='\0'){
            if (*(colonne+i)==','){
                var[c]='\0';
                if(!strcmp(var,"*")){
                    arg[0]=1;
                    arg[1]=1;
                    arg[2]=1;
                } else if(!strcmp(var,"var1")){
                    arg[0]=1;
                } else if(!strcmp(var,"var2")){
                    arg[1]=1;
                } else if(!strcmp(var,"var3")){
                    arg[2]=1;
                }
                free(var);
                var = calloc(LG_MOT,sizeof(char));
                c=0;
            } else { 
                *(var+c)= *(colonne+i);
                c++;
            }
            i++;
        }
        var[c]='\0';
        if(!strcmp(var,"*")){
            arg[0]=1;
            arg[1]=1;
            arg[2]=1;
        } else if(!strcmp(var,"var1")){
            arg[0]=1;
        } else if(!strcmp(var,"var2")){
            arg[1]=1;
        } else if(!strcmp(var,"var3")){
            arg[2]=1;
        }
        free(var);
}


int detect_condition(char* condition, char* cond, int socketClient){
    int num_var=-1;
    char* var = calloc(LG_MOT,sizeof(char));
    int i = 0;
    int c = 0;
    int condition_find=0;
    while(*(condition+i)!='\0' && *(condition+i)!='\n'){
        if (*(condition+i)=='='){
            *(var+i)='\0';
            if(!strcmp(var,"var1")){
                num_var=1;
            } else if(!strcmp(var,"var2")){
                num_var=2;
            } else if(!strcmp(var,"var3")){
                num_var=3;
            } else {
                printf("Erreur : nom de colonne non reconnu\n");
                free(var);
                return -1;
            }
            free(var);
            i++;
            condition_find = 1;
        }
        if(!condition_find) { 
            *(var+i)= *(condition+i);
            i++;
        } else {
            *(cond+c)= *(condition+i);
            c++;
            i++;
        }
    }
    cond[c]='\0';
    return num_var;
}

int test_ligne(char *vars[4], int num_var, char* cond){
    if(num_var==-1){
        return 1;
    }
    if(strcmp(vars[num_var],cond)){
        return 0;
    }
    return 1;
}

void recup_vars(char* ligne, char* vars[4]){
    int nv=0;
    int i=0;
    int c=0;
    while(*(ligne+i)!='\n'){
        if(*(ligne+i)==','){
            nv++;
            i++;
            c=0;
        } else {
            *(vars[nv]+c)= *(ligne+i);
            i++;
            c++;
        }
    }
}

void affiche_vars(char* vars[4], int arg[3], int socketClient){
    printf("%s",vars[0]);
    for(int i=0;i<3;i++){
        if(arg[i]){
            printf(",%s",vars[i+1]);
        }
    }
    printf("\n");
}

int sel(char** mots, int c, int socketClient){
    if(c<4){
        printf("Nombre argument non valide\n");
        return 0;
    }

    char *table = calloc(LG_MOT,sizeof(char));
    int i = 0;
    while(*(mots[3]+i)!='\0' && *(mots[3]+i)!='\n'){
        *(table+i)=*(mots[3]+i);
        i++;
    }
    table[i]='\0';
    strcat(table,".txt");
    //On fait des testes d'erreures de saisies
    if (strcmp(mots[2],"FROM")){
        printf("Mauvais format : absence de 'FROM' en 3eme position\n");
    } else if (!fichier_existe(table)){
        printf("Opération impossible : La table n'existe pas\n");
    } else {
        // si pas d'erreur 
        FILE* f = fopen(table,"r");

        // test sur les colonnes a afficher 
        int arg[3] = {0,0,0};
        detect_var(mots[1],arg);
        // fin colonne a afficher 
        int num_var = -1;
        char* cond = NULL;
        if(c>=6){
            if(!strcmp(mots[4],"WHERE")){
                cond = calloc(LG_MOT,sizeof(char));
                num_var = detect_condition(mots[5],cond,socketClient);
            }
        }
        //On parcourt le fichier et on affiche les donnés voulues
        char *ligne = NULL;
        size_t len = 0;
        getline(&ligne,&len,f);
        while (getline(&ligne,&len,f) != -1){
            char *id = calloc(LG_MOT,sizeof(char));
            char *var1 = calloc(LG_MOT,sizeof(char));
            char *var2 = calloc(LG_MOT,sizeof(char));
            char *var3 = calloc(LG_MOT,sizeof(char));
            char* vars[4] = {id,var1,var2,var3};
            recup_vars(ligne,vars);
            if(test_ligne(vars,num_var,cond)){
                affiche_vars(vars,arg,socketClient);
            }
            free(id);
            free(var1);
            free(var2);
            free(var3);
        }


        fclose(f);
    }
    free(table);
    return 1;
}


int del(char** mots, int c, int socketClient){
    printf("debut del()\n");
    if(c<3){
        printf("Nombre argument non valide\n");
        return 0;
    }

    char *table = calloc(LG_MOT,sizeof(char));
    int i = 0;
    while(*(mots[2]+i)!='\0' && *(mots[2]+i)!='\n'){
        *(table+i)=*(mots[2]+i);
        i++;
    }
    table[i]='\0';
    strcat(table,".txt");
    //On fait des testes d'erreures de saisies
    if (strcmp(mots[1],"FROM")){
        printf("Mauvais format : absence de 'FROM' en 2eme position\n");
    } else if (!fichier_existe(table)){
        printf("Opération impossible : La table n'existe pas\n");
    } else {
        // si pas d'erreur 
        printf("pas d'erreur de format\n");
        FILE* f = fopen(table,"r");

        int num_var = -1;
        char* cond = NULL;
        if(c>=5){
            printf("yololo\n");
            if(!strcmp(mots[3],"WHERE")){
                cond = calloc(LG_MOT,sizeof(char));
                num_var = detect_condition(mots[4],cond,socketClient);
            }
        }
        //On parcourt le fichier et on affiche les donnés voulues
        char *ligne = NULL;
        size_t len = 0;
        FILE* ftmp = fopen("tmp.txt","w");
        while (getline(&ligne,&len,f) != -1){
            char *id = calloc(LG_MOT,sizeof(char));
            char *var1 = calloc(LG_MOT,sizeof(char));
            char *var2 = calloc(LG_MOT,sizeof(char));
            char *var3 = calloc(LG_MOT,sizeof(char));
            char* vars[4] = {id,var1,var2,var3};
            recup_vars(ligne,vars);
            printf("cond %s\n",cond);
            if(!test_ligne(vars,num_var,cond)){
                printf("inside\n");
                fputs(ligne,ftmp);
            }
            free(id);
            free(var1);
            free(var2);
            free(var3);
        }
        fclose(ftmp);
        fclose(f);
        remove(table);
        rename("tmp.txt",table);
    }
    free(table);
    printf("fin del()\n");
    return 1;
}



