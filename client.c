#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>

#define REQUEST_SIZE 400

int main(){


    typedef struct User {
        int age;
    } User;

    // Création du server
    int socketClient = socket(AF_INET,SOCK_STREAM,0);

    struct sockaddr_in addrClient;
    addrClient.sin_family = AF_INET; 
    addrClient.sin_addr.s_addr = inet_addr("127.0.0.1"); 
    addrClient.sin_port = htons(3000);


    // connection du client
    socklen_t addr_size = sizeof(addrClient); 
    int connecter = connect(socketClient, (struct sockaddr*) &addrClient, addr_size);
    printf("connecté = %d\n",connecter);
    char *request = malloc(REQUEST_SIZE);
    do{
        printf("Entrez une commande\n");
        fgets(request,REQUEST_SIZE,stdin);
       // printf("%s\n",request);
        send(socketClient,request,REQUEST_SIZE,0);
    } while (strcmp(request,"q"));
    free(request);
    

    close(socketClient);
    printf("close\n");
    // Fin création du server
    return 0;
}