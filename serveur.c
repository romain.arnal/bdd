#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>
#include "bdd.h"

#define NB_JETONS 1000

void traitementRequest(char *, pthread_mutex_t *, sem_t*);
int createtable(char **, int);
int droptable(char **,int);
void insert(char **,int);
int sel(char **,int);
int del(char **,int);
int fichier_existe(char *);


int main(){
    printf("debut\n");

    // création du verrou
    pthread_mutex_t mutex_lock = PTHREAD_MUTEX_INITIALIZER;
    const pthread_mutexattr_t * lock_attr = NULL;
    pthread_mutex_init( &mutex_lock, NULL);
    // création du sémaphore
    sem_t sem;
    int pshared = 1; 
    unsigned int value = NB_JETONS;
    sem_init(&sem, 0, value);
    int n;
    sem_getvalue(&sem,&n);

    // Création du server
    int socketServeur = socket(AF_INET,SOCK_STREAM,0);

    struct sockaddr_in address;
    address.sin_family = AF_INET; 
    address.sin_addr.s_addr = inet_addr("127.0.0.1"); 
    address.sin_port = htons(3000);

    bind(socketServeur, (struct sockaddr*) &address, sizeof(address));
    listen(socketServeur,3);
    printf("listen\n");

    

    while (1)
    {
        struct sockaddr_in addr;
        socklen_t addr_size = sizeof(addr); 

        // connection du client
        int socketClient = accept(socketServeur, (struct sockaddr*) &addr, &addr_size);
        printf("accept\n");
        pid_t pid = fork();
        if (pid==-1){
            printf("erreur de fork()\n");
        } else if (pid==0){
            printf("SocketClient = %d\n", socketClient);
            char *request = malloc(REQUEST_SIZE);
            do {
                recv(socketClient,request,REQUEST_SIZE,0);
                printf("Requete du client : %s\n",request);
                traitementRequest(request, &mutex_lock, &sem);
            } while (strcmp(request,"q"));
            free(request);
            close(socketClient);
        }
    }
    
    sem_destroy(&sem);
    close(socketServeur);
    printf("close\n");
    
    return 0;
}

void wait_sem_is_empty(sem_t* sem){
    int jeton_restant = 5;
    int t;
    while (jeton_restant != NB_JETONS)
    {
        t = sem_getvalue(sem,&jeton_restant);
        printf("wait for all sem jetons : %d, %d, %d\n",jeton_restant,t,NB_JETONS);
    }
}

void traitementRequest(char *request, pthread_mutex_t * mutex_lock, sem_t * sem){
    char **mots = malloc(sizeof(char*)*NB_MOT_MAX);
    char *buffer = strtok(request," ");
    int compteur = 0;

    while(buffer!=NULL){
        mots[compteur] = malloc(LG_MOT*sizeof(char));
        strcpy(mots[compteur],buffer);
        buffer = strtok(NULL," ");
        compteur+=1;
    }
    pthread_mutex_lock(mutex_lock);
    if(!strcmp(mots[0],"CREATE")){
        wait_sem_is_empty(sem);
        createtable(mots,compteur); 
        pthread_mutex_unlock(mutex_lock);
    } else if (!strcmp(mots[0],"DROP")){
        wait_sem_is_empty(sem);
        droptable(mots,compteur);
        pthread_mutex_unlock(mutex_lock);
    } else if(!strcmp(mots[0],"INSERT")){
        wait_sem_is_empty(sem);
        insert(mots,compteur);
        pthread_mutex_unlock(mutex_lock);
    } else if(!strcmp(mots[0],"DELETE")){
        wait_sem_is_empty(sem);
        del(mots,compteur);
        pthread_mutex_unlock(mutex_lock);
    } else if(!strcmp(mots[0],"SELECT")){
        sem_wait(sem);
        pthread_mutex_unlock(mutex_lock);
        sel(mots,compteur);
        sem_post(sem);
    }
    for(int i=0;i<compteur;i++){
        free(mots[i]);
    }
    free(mots);
}

